﻿using System;
using System.Configuration;
using System.Globalization;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Mail;

public partial class ingserver : System.Web.UI.Page
{
    // oggetti per accedere ad un database generico che incapsulano le versioni specifiche di: 
    // Connection, Command e Datareader. Realizzato da De Biase
    // per facilitare l'intercambiabilità dei database. Il sorgente si trova
    // nel file dbmysql.cs
    dbhelper db = new dbhelper();
    object cnn, rs;

    /// <summary>
    /// router del server. Qui interpreto il parametro op che specifica l'operazione da eseguire e 
    /// richiamo l'handler con gli opportuni parametri. Si usa l'oggetto predefinito Request per 
    /// ottenere i parametri della richiesta HTTP
    /// </summary>
    /// <param name="sender">oggetto che ha generato l'evento</param>
    /// <param name="e">parametri dell'evento</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        // Request è l'oggetto predefinito che restituisce i parametri della richiesta http
        string op = Request["op"];

        if (op != null)
        {
            if (op == "2")
                serverFase1(Request["ccli"], Request["dcli"]);
            else if (op == "3")
                serverValidaPIN(Request["pin"]);
            else if (op == "11")
                testCifratura();
            else if (op == "12")
                testMailing();
        }
    }
    /// <summary>
    /// Controlla esistenza del codice cliente. Se non esiste ritorna errore al client
    /// </summary>
    /// <param name="ccli"></param>
    /// <param name="dcli"></param>
    protected void serverFase1(string ccli, string dcli)
    {
        // json=stringa che serve a restituire la struttura dati in risposta alle richieste di ajax;
        // sql=stringa sql per leggere/scrivere i dati nel database
        string json, sql, idutente = "", pinutente = "";
        int ctenta = 1;
        if (Session["ctenta"] != null)
            ctenta = (int)Session["ctenta"];
        // todo: cerca se esiste il ccli nel database
        sql = "select id, pin from utenti where codcli='" + ccli + "' and data='" + dcli + "'";
        // mi connetto al database
        cnn = db.InitConnection();
        if (cnn != null)
        {
            // lancio il comando per leggere dal database
            rs = db.RunSQLrs(sql, cnn);
            if (rs!=null && db.Read(rs))
            {
                // se arriva qui ho letto almeno una riga ed è presente nel datareader (rs) 
                idutente = db.gets(rs, "id");
                pinutente = db.gets(rs, "pin");
            }
        }
        db.CloseConnection(cnn);
        // se esiste e se la data di nascita è giusta ritorno OK e mantengo ccli e pin in una variabile di sessione.
        // (per maggiore sicurezza non lo faccio viaggiare avanti e indietro nel round-trip)
        // se non esiste ritorno errore ed incremento il numero dei tentativi
        // anche il contatore lo salvo in una variabile di sessione
        if (ctenta < 4 && idutente != "")
        {
            Session["ctenta"] = 1;
            Session["pin"] = pinutente;
            Session["id"] = idutente;
            json = "{}";
        }
        else if (ctenta < 4)
        {
            Session["ctenta"] = ctenta + 1;
            Session["pin"] = null;
            json = "{err:\"Codice Cliente errato. Hai ancora " + (4 - ctenta) + " tentativi.\"}";
        }
        else
        {
            Session["ctenta"] = ctenta + 1;
            Session["pin"] = null;
            json = "{err:\"Ritenta tra mezzora.\"}";
        }
        rispondi(json);
    }
    /// <summary>
    /// controllo la validità del PIN
    /// se è sbagliato ritorno errore e incremento il contatore
    /// </summary>
    /// <param name="pin"></param>
    protected void serverValidaPIN(string pin)
    {
        string json, cfrpin = "";
        int ctenta = 1;
        if (Session["ctenta"] != null)
            ctenta = (int)Session["ctenta"];
        // ricavo ctenta e pin dalle variabili di sessione
        if (Session["pin"] != null)
            cfrpin = Session["pin"].ToString();
        if (ctenta < 4 && confronta(pin, cfrpin))
            json = "{}";
        else if (ctenta < 4)
        {
            Session["ctenta"] = ctenta + 1;
            json = "{err:\"PIN errato. Hai ancora " + (4 - ctenta) + " tentativi.\"}";
        }
        else
        {
            Session["ctenta"] = ctenta + 1;
            json = "{err:\"Ritenta tra mezzora.\"}";
        }
        rispondi(json);
    }
    /// <summary>
    /// Esegue un test di cifratura per dimostrare l'uso delle classy del 
    /// namespace System.Security.Cryptography;
    /// </summary>
    void testCifratura()
    {
        // todo
    }
    /// <summary>
    /// Esegue un test di invio di una email da un web server
    /// </summary>
    void testMailing()
    {
        // todo
    }
    /// <summary>
    /// prepara la risposta per il client
    /// </summary>
    /// <param name="json"></param>
    protected void rispondi(string json)
    {
        Response.Clear();
        // la risposta è di tipo JSON invece che html. Il set di caratteri è unicode
        Response.ContentType = "application/json; charset=utf-8";
        Response.Write("(" + json + ")");
        Response.End();
    }
    /// <summary>
    /// confronta 2 pin a meno degli '*'
    /// </summary>
    /// <param name="pin"></param>
    /// <param name="cfrpin"></param>
    /// <returns></returns>
    bool confronta(string pin, string cfrpin)
    {
        // todo: confronta se i 2 pin sono uguali a meno degli '*'
        int k, nasterischi = 0;
        bool ok = true;
        for (k = 0; k < 6; k++)
        {
            if (pin.Substring(k, 1) != "*" && pin.Substring(k, 1) != cfrpin.Substring(k, 1))
                ok = false;
            if (pin.Substring(k, 1) == "*")
                nasterischi++;
        }
        return (nasterischi == 3) ? ok : false;
    }
    /// <summary>
    /// invio email con parametri configurati in web.config
    /// </summary>
    /// <param name="text">testo della email</param>
    /// <param name="subject">oggetto della email</param>
    /// <param name="addr">indirizzo di destinazione (cc se esiste mainaddr)</param>
    /// <param name="mainaddr">indirizzo principale di destinazione (facoltativo)</param>
    /// <returns>errore o null</returns>
    protected string mysend(string text, string subject, string addr, string mainaddr)
    {
        string ret = null,
            enableSSL = ConfigurationManager.AppSettings["ENABLESSL"],
            login = ConfigurationManager.AppSettings["LOGIN"],
            pwd = ConfigurationManager.AppSettings["PASSWORD"],
            sport = ConfigurationManager.AppSettings["SMTPPORT"],
            chost = ConfigurationManager.AppSettings["CHOST"],
            caddress = ConfigurationManager.AppSettings["CADDRESS"],
            cdispname = ConfigurationManager.AppSettings["CDISPNAME"];

        if (addr == null)
            addr = ConfigurationManager.AppSettings["SAMPLEADDR"];
        try
        {
            MailMessage mm = new MailMessage();
            SmtpClient smtp = new SmtpClient(chost);
            if (sport != "")
                smtp.Port = Convert.ToInt32(sport);
            if (login != "")
            {
                smtp.Credentials = new System.Net.NetworkCredential(login, pwd);
                if (enableSSL != "")
                    smtp.EnableSsl = true;
            }
            if (mainaddr != null && mainaddr != "")
            {
                mm.To.Add(mainaddr);
                mm.CC.Add(addr);
            }
            else
                mm.To.Add(addr);
            //mm.Bcc.Add("ndebiase@intres.it");
            mm.From = new MailAddress(caddress, cdispname);
            mm.Subject = subject;
            mm.IsBodyHtml = false;
            mm.Body = text;
            smtp.Send(mm);
        }
        catch (Exception ex)
        {
            ret = ex.Message;
        }
        return ret;
    }
}
