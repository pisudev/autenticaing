using System;
using System.Configuration;
using System.Globalization;
using System.Data;
using System.Data.OleDb;
using MySql.Data.MySqlClient;

using System.Security.Cryptography;
using System.IO;
using System.Text;

/// <summary>
/// accesso ai database SOLO MySql
/// </summary>
public class dbhelper
{
    #region DBHELPER
    //
    // esempi di connessione OLEDB
    //
    // Provider=MSDAORA; Data Source=ORACLE8i7; User ID=OLEDB; Password=OLEDB
    // Provider=Microsoft.Jet.OLEDB.4.0; Data Source=c:\bin\LocalAccess40.mdb;
    // Provider=SQLOLEDB;Data Source=MySQLServer;Integrated Security=SSPI;		
    //
    // esempi di connessione ODBC
    //
    // Driver={SQL Server};Server=MyServer;UID=sa;PWD=sqLs$5xr;Database=Northwind;
    // Driver={Microsoft ODBC for Oracle};Server=ORACLE8i7;UID=odbcuser;PWD=odbc$5xr
    // Driver={Microsoft Access Driver (*.mdb)};DBQ=c:\bin\nwind.mdb
    // Driver={Microsoft Excel Driver (*.xls)};DBQ=c:\bin\book1.xls
    // Driver={Microsoft Text Driver (*.txt; *.csv)};DBQ=c:\bin
    // DSN=dsnname
    // stringa di connessione di default. Serve per non farla apparire in tutti i sorgenti tranne qui
    string defaultcnnstring = ConfigurationManager.AppSettings["mycnn"];
    CultureInfo neutral = new CultureInfo("");
    // contiene l'eventuale errore di accesso al database
    public string LastErr;

    /// <summary>
    /// Setup connessione generica ad un database
    /// </summary>
    /// <param name="connstr"></param>
    /// <returns></returns>
    public Object InitConnection()
    {
        return InitConnection(null);
    }
    public Object InitConnection(string connstr)
    {
        Object cnn = null;

        try
        {
            if (connstr == null)
                connstr = defaultcnnstring;
            if (connstr.ToLower().StartsWith("provider"))
            {
                cnn = new OleDbConnection(connstr);
                ((OleDbConnection)cnn).Open();
            }
            else if (connstr.ToLower().EndsWith(".mdb"))
            {
                cnn = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + connstr + ";");
                ((OleDbConnection)cnn).Open();
            }
            else
            {
                cnn = new MySqlConnection(connstr);
                ((MySqlConnection)cnn).Open();
            }
        }
        catch (Exception e)
        {
            LastErr = e.Message;
        }
        return cnn;
    }
    /// <summary>
    /// Chiude la connessione OleDb
    /// </summary>
    /// <param name="cnn"></param>
    public void CloseConnection(OleDbConnection cnn)
    {
        if (cnn != null)
            cnn.Close();
    }
    /// <summary>
    /// Chiude la connessione Generica
    /// </summary>
    /// <param name="cnn"></param>
    public void CloseConnection(Object cnn)
    {
        if (cnn != null)
        {
            if (cnn.GetType() == typeof(OleDbConnection))
                ((OleDbConnection)cnn).Close();
            else
                ((MySqlConnection)cnn).Close();
        }
    }
    /// <summary>
    /// effettua una query di selezione con connessione generica
    /// </summary>
    /// <param name="sql"></param>
    /// <param name="cnn"></param>
    /// <returns>null=errore, DataReader altrimenti</returns>
    public Object RunSQLrs(string sql, Object cnn)
    {
        if (cnn.GetType() == typeof(OleDbConnection))
            return RunSQLrs(sql, (OleDbConnection)cnn);
        else
            return RunSQLrs(sql, (MySqlConnection)cnn);
    }
    /// <summary>
    /// effettua una query di selezione con OleDb
    /// </summary>
    /// <param name="sql"></param>
    /// <param name="cnn"></param>
    /// <returns>null=errore, DataReader altrimenti</returns>
    public OleDbDataReader RunSQLrs(string sql, OleDbConnection cnn)
    {
        try
        {
            OleDbCommand cmd = new OleDbCommand(sql, cnn);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.Clear();
            return cmd.ExecuteReader();
        }
        catch (Exception e)
        {
            LastErr = e.Message;
            return null;
        }
    }
    /// <summary>
    /// effettua una query di selezione con MySql
    /// </summary>
    /// <param name="sql"></param>
    /// <param name="cnn"></param>
    /// <returns>null=errore, DataReader altrimenti</returns>
    public MySqlDataReader RunSQLrs(string sql, MySqlConnection cnn)
    {
        try
        {
            MySqlCommand cmd = new MySqlCommand(sql, cnn);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.Clear();
            return cmd.ExecuteReader();
        }
        catch (Exception e)
        {
            LastErr = e.Message;
            return null;
        }
    }
    /// <summary>
    /// effettua una query di comando con connessione generica
    /// </summary>
    /// <param name="sql"></param>
    /// <param name="cnn"></param>
    /// <returns>-1=errore, x=numero di righe influenzate</returns>
    public int RunSQL(string sql, Object cnn)
    {
        if (cnn.GetType() == typeof(OleDbConnection))
            return RunSQL(sql, (OleDbConnection)cnn);
        else
            return RunSQL(sql, (MySqlConnection)cnn);
    }
    /// <summary>
	/// effettua una query di comando con ODBC, OLEDB, MySQL
	/// </summary>
	/// <param name="sql"></param>
	/// <param name="cnn"></param>
	/// <returns>-1=errore, x=numero di righe influenzate</returns>
    public int RunSQL(string sql, OleDbConnection cnn)
    {
        try
        {
            OleDbCommand cmd = new OleDbCommand(sql, cnn);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.Clear();
            return cmd.ExecuteNonQuery();
        }
        catch (Exception e)
        {
            LastErr = e.Message;
            return -1;
        }
    }
    public int RunSQL(string sql, MySqlConnection cnn)
    {
        try
        {
            MySqlCommand cmd = new MySqlCommand(sql, cnn);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.Clear();
            return cmd.ExecuteNonQuery();
        }
        catch (Exception e)
        {
            LastErr = e.Message;
            return -1;
        }
    }
    /// <summary>
    /// Mappa la read del recordset generico
    /// </summary>
    /// <param name="rs"></param>
    /// <returns></returns>
    public bool Read(Object rs)
    {
        if (rs.GetType() == typeof(OleDbDataReader))
            return ((OleDbDataReader)rs).Read();
        else
            return ((MySqlDataReader)rs).Read();
    }
    /// <summary>
    /// Mappa la close del recordset generico
    /// </summary>
    /// <param name="rs"></param>
    /// <returns></returns>
    public void Close(Object rs)
    {
        if (rs != null)
        {
            if (rs.GetType() == typeof(OleDbDataReader))
                ((OleDbDataReader)rs).Close();
            else
                ((MySqlDataReader)rs).Close();
        }
    }
    /// <summary>
    /// legge il campo specificato dal recordset generico
    /// </summary>
    /// <param name="rs"></param>
    /// <param name="fld"></param>
    /// <returns></returns>
    public string gets(Object rs, string fld)
    {
        if (rs.GetType() == typeof(OleDbDataReader))
            return (((OleDbDataReader)rs)[fld] == System.DBNull.Value) ? "" : ((OleDbDataReader)rs)[fld].ToString();
        else
            return (((MySqlDataReader)rs)[fld] == System.DBNull.Value) ? "" : ((MySqlDataReader)rs)[fld].ToString();
    }
    #endregion
    #region SHOTS
    /// <summary>
    /// Registra una azione nella tabella shots e memorizza anche l'indirizzo IP del client (proxy)
    /// </summary>
    /// <param name="tipo">tipo di azione: 0=login, 1=like pezzo, 2=like autore, 3=lettura pezzo
    /// <param name="idu">id utente</param>
    /// <param name="ido">id oggetto</param>
    /// <param name="dato">dettaglio</param>
    /// <param name="ip">client ip address (from Request.UserHostAddress)</param>
    /// <param name="cnn"></param>
    public void registraShot(string tipo, string idu, string ido, string dato, string ip, object cnn)
    {
        string sql;
        int ret;
        if (tipo == "3")
        {
            // nel caso di lettura pezzo aggiorno la data e ip di ciascun lettore
            sql = "update shots set info=1, dtagg=now(), ip='" + ip + "' where tipo=3 and idu=" + idu + " and ido=" + ido;
            ret = RunSQL(sql, cnn);
            if (ret == 0)
            {
                sql = "insert into shots (id,idu,tipo,ido,dtagg,ip,info) values (" +
                    "0," + idu + "," + tipo + "," + ido + ",now(),'" + ip + "',1)";
                ret = RunSQL(sql, cnn);
            }
        }
        else
        {
            sql = "insert into shots (id,idu,tipo,ido,dtagg,ip,info) values (" +
                    "0," + idu + "," + tipo + "," + ido + ",now(),'" + ip + "'," + dato + ")";
            RunSQL(sql, cnn);
        }
        // se � definito il database ipinfo registra ip
        registraIP(ip, "", "", "", "", "", cnn);
    }
    /// <summary>
    /// registra IP di chi ha fatto l'azione
    /// </summary>
    /// <param name="ip"></param>
    /// <param name="city"></param>
    /// <param name="region"></param>
    /// <param name="country"></param>
    /// <param name="loc"></param>
    /// <param name="org"></param>
    /// <param name="cnn"></param>
    public void registraIP(string ip, string city, string region, string country, string loc, string org, object cnn)
    {
        string sql = "insert into ipinfo (ip,lastagg,city,region,country,loc,org) values ('" +
            ip + "',now(),'" +
            city + "','" +
            region + "','" +
            country + "','" +
            loc + "','" +
            org + "')";
        RunSQL(sql, cnn);
        LastErr = null;
    }
    public string getIPList(object cnn)
    {
        string json = "ipinfo:[", sql = "select ip from ipinfo where country=''";
        object rs = RunSQLrs(sql, cnn);
        while (rs != null && Read(rs))
            json += "{ip:\"" + gets(rs, "ip") + "\"},";
        json += "]";
        return json;
    }
    #endregion
    #region SUPPORTO
    /// <summary>
    /// Permette di usare gli alias dei nomi delle tabelle creati in web.config.
    /// Ho usato questo metodo per creare dei siti-fotocopia che utilizzano un solo database.
    /// Definisco gli alias delle tabelle nella sezione appSettings. Es: <add key="artTAB" value="artari"/>
    /// </summary>
    /// <param name="originaltabname"></param>
    /// <returns></returns>
    public string tb(string originaltabname)
    {
        if (ConfigurationManager.AppSettings[originaltabname + "TAB"] != null)
            return ConfigurationManager.AppSettings[originaltabname + "TAB"];
        else
            return originaltabname;
    }
    /// <summary>
    /// bonifica una stringa
    /// </summary>
    /// <param name="t"></param>
    /// <returns></returns>
    public string bonifica(string t)
    {
        return t.Replace("\\", "\\\\").Replace("'", "\\\'").Replace("�", "\\\'");
    }
    /// <summary>
    /// chiama bonificajson, inoltre sostituisce tutti i caratteri che danno fastidio
    /// a 3cad come la virgola.
    /// </summary>
    /// <param name="t"></param>
    /// <returns></returns>
    public string bonificaj3cad(string t)
    {
        string qq = t.Replace(",", ".");
        return bonificajson(qq);
    }
    /// <summary>
    /// consente solo apice singolo. Elimina \n, apici doppi e backtick.
    /// standard JSON vuole apici doppi come delimitatore di stringhe
    /// </summary>
    /// <param name="t"></param>
    /// <returns></returns>
    public string bonificajson(string t)
    {
        string qq = t.Replace("�", "'").Replace("\"", "\\\"").Replace("\r\n", " ").Replace("\n", " ");
        return qq;
    }
    /// <summary>
    /// Sistema i nomi dei file
    /// </summary>
    /// <param name="fn"></param>
    /// <returns></returns>
    public string bonificafilename(string fn)
    {
        string qq = fn.Trim().Replace(" ", "_").Replace("/", "_").Replace(".", "_").Replace("+", "_").Replace("#", "_");
        return qq;
    }
    public double FromNeutral(string numero)
    {
        try
        {
            return Convert.ToDouble(numero, neutral.NumberFormat);
        }
        catch
        {
            return 0;
        }
    }
    #endregion
    #region CRYPTO
    /// <summary>
    /// cifratura stringa con il metodo Rijndael (� un AES)
    /// </summary>
    /// <param name="s">stringa da cifrare</param>
    /// <returns>stringa cifrata</returns>
    public string encrypt(RijndaelManaged rij, string s)
    {
        ICryptoTransform cri = rij.CreateEncryptor();
        // cri � il cifratore usato come motore del cryptostream
        MemoryStream ms = new MemoryStream();
        // ms � il memory stream usato come output del cryptostream 
        CryptoStream cs = new CryptoStream(ms, cri, CryptoStreamMode.Write);
        byte[] ba = Encoding.UTF8.GetBytes(s);
        // ba � il byte array che contiene la stringa da cifrare (gli stream operano solo su byte array). E' l'input del cryptostream
        cs.Write(ba, 0, ba.Length);
        cs.FlushFinalBlock();
        // ora ms contiene la stringa cifrata. Devo trasformare il memory stream in un base64String. Restituisco una stringa cifrata al chiamante
        return Convert.ToBase64String(ms.ToArray());
    }
    /// <summary>
    /// decifratura stringa con il metodo Rijndael (� un AES)
    /// </summary>
    /// <param name="es">stringa base64 da decifrare</param>
    /// <returns></returns>
    public string decrypt(RijndaelManaged rij, string es)
    {
        // trasformo la stringa base64 in byte array
        byte[] ba = Convert.FromBase64String(es);
        // ba � il byte array da decifrare. E' l'input del cryptostream
        ICryptoTransform decri = rij.CreateDecryptor();
        // decri � il decifratore usato come motore del cryptostream
        MemoryStream ms = new MemoryStream(ba);
        // ms � il memory stream usato come output del cryptostream 
        CryptoStream cs = new CryptoStream(ms, decri, CryptoStreamMode.Read);
        //cs.Read(buf, 0, ba.Length);
        StreamReader sr = new StreamReader(cs,Encoding.UTF8);
        // ora ms contiene la stringa decifrata. Devo trasformare il memory stream in un UTF8 string. Restituisco la stringa decifrata al chiamante
        //return Encoding.UTF8.GetString(buf);
        return sr.ReadToEnd();
    }
    /// <summary>
    /// Restituisce un oggetto Rijndael (AES) per effettuare la cifratura
    /// Rijndael � un algoritmo di cifratura a chiave simmetrica. E' il generatore di AES.
    /// Si personalizza con una key e un IV (initialization vector)
    /// Se non si conoscono entrambi (key, IV) � impossibile da decifrare
    /// </summary>
    /// <returns></returns>
    public object getrij()
    {
        const int L1 = 44;
        string key;
        RijndaelManaged rij = new RijndaelManaged();
        // rij ora contiene un algoritmo con key e IV generato casualmente dal clock del computer.
        // E' necessario memorizzare key e IV per poter decifrare ci� che si � cifrato.
        // BISOGNA MANTENERE SEGRETI key e IV
        key = ConfigurationManager.AppSettings["mycr"];
        // Le righe commentate servono per leggere Key e IV (Initializing Vector) della cifratura.
        // Questa operazione va fatta una sola volta per ottenere un valore fisso di key e IV
        // da memorizzare in web.config per usare sempre la stessa chiave di cifratura per il sito web
        //string qq = Convert.ToBase64String(rij.IV);
        //string aa = Convert.ToBase64String(rij.Key);
        //key = aa + qq;
        if (key == null)
            return null;
        // estraggo key e IV dalla variabile di configurazione
        rij.Key = Convert.FromBase64String(key.Substring(0, L1));
        rij.IV = Convert.FromBase64String(key.Substring(L1));
        // ora rij � personalizzato con key e IV
        return rij;
    }
    #endregion
}
