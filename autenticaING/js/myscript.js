﻿/**
 * in javascript si possono definire variabili globali. Per evitare l'inquinamento del namespace conviene creare
 * un contenitore unico per tutte le variabili della applicazione.
 * */
var SES = {
    // array delle 20 combinazioni di 3 uni su 6 cifre binarie
    cod3o6: [],
    pin: []
};

/**
 * entry point del programma. Quando la pagina viene caricata completamente viene chiamata la funzione che appare come
 * parametro (init). $ è un sinonimo della funzione jquery() che è la funzione principale della omonima libreria.
 * JQquery è la libreria per accedere ai tag della pagina html. Si può fare a meno, ma la sintassi di jquery è più facile.
 * La libreria si include nel file html. Per approfondire jquery: http://www.jquery.com
 * @param {any} init - funzione richiamata quando tutta la pagina html viene caricata
 * */
$(document).ready(init);

function init() {
    // nascondo il tastierino
    $("#inputpin").hide();
    // mostro la richiesta del codice cliente
    $("#inputcodicecli").show();
    // calcola array delle 20 combinazioni di 3 uni su 6 cifre binarie
    makecod3of6();
}

/**
 * quando premo il bottone Avanti chiamo questa funzione
 * */
function avanti1() {
    var ccli = $("#ccli").val(),
        dcli = $("#dcli").val();
    // richiamo il server con ajax e gli passo il codice con la data di nascita
    $.ajax({
        url: "ingserver.aspx", type: "POST", dataType: "json",
        data: { op: 2, ccli: ccli, dcli: dcli }
    }).complete(function (data) {
        // controllo che la richiesta sia andata a buon fine
        if (data.status == 200 && data.readyState == 4) {
            SES.data = eval(data.responseText);
            if (SES.data.err != null)
                alert(SES.data.err);
            else {
                // operazioni da fare quando il server ha risposto
                // eseguo il DOM manipulation descritto nel markdown
                // nascondo la richiesta del codice cliente
                $("#inputcodicecli").hide();
                // mostro il tastierino
                $("#inputpin").show();
                randomizzaTastierino();
            }
        }
    });
}

/**
 * cancella il pin e ricomincia l'input dalla prima cifra 
 * */
function cancella() {
    // ricomincia l'input dalla prima cifra
    randomizzaTastierino();
}

/**
 * invia il pin per il controllo di validità
 * */
function avanti2() {
    // con questa istruzione completo il pin se è incompleto
    tast("*");
    // richiamo il server con ajax e gli passo il pin (parziale)
    $.ajax({
        url: "ingserver.aspx", type: "POST", dataType: "json",
        data: { op: 3, pin:SES.pin.join("") }
    }).complete(function (data) {
        // controllo che la richiesta sia andata a buon fine
        if (data.status == 200 && data.readyState == 4) {
            SES.data = eval(data.responseText);
            if (SES.data.err != null)
                alert(SES.data.err);
            else {
                // operazioni da fare quando il server ha risposto
                // eseguo il DOM manipulation descritto nel markdown
                alert("OK. SEI LOGGATO!");
                // ricomincia il test
                init();
            }
        }
    });
}

/**
 * crea un array di 20 numeri da 0 a 63 (6 cifre binarie) con 3 uni nella rappresentazione binaria
 */
function makecod3of6() {
    var str, k;
    // azzera gli array
    SES.cod3o6 = [];
    SES.pin = [];
    for (k = 0; k < 64; k++) {
        // trasforma un numero intero in binario con 6 cifre. se il numero contiene 3 '1' lo prende
        str = k.toString(2).padStart(6, '0');
        if (str.split('1').length == 4)
            SES.cod3o6.push(k);
    }
}

/**
 * Effettua la visualizzazione dei tasti del tastierino mischiando i numeri.
 * Fa apparire gli * nel campo di input.
 **/
function randomizzaTastierino() {
    var i, j, k, giamesso, aster;
    SES.x = [];
    SES.pin = [];
    for (i = 0; i < 10;) {
        k = Math.floor(Math.random() * 10);
        giamesso = false;
        for (j = 0; j < SES.x.length; j++) {
            if (SES.x[j] == k) {
                giamesso = true;
                break;
            }
        }
        if (!giamesso) {
            SES.x[i] = k;
            i++;
        }
    }
    // faccio apparire i numeri sul tastierino con jquery
    for (i = 0; i < 10; i++) {
        $("#c" + i).text(SES.x[i]);
    }
    // visualizzo gli asterischi nel campo
    i = Math.floor(Math.random() * 20);
    aster = SES.cod3o6[i].toString(2).padStart(6, '0');
    for (k = 0; k < 6; k++) {
        if (aster.substring(k, k + 1) == "0")
            $("#p" + k).text("");
        else
            $("#p" + k).text("*");
    }
}

/**
 * interpreta il tasto digitato sul tastierino e riempie il prossimo spazio vuoto del PIN
 * @param {any} n - numero d'ordine del tasto oppure '*' per terminare l'inserimento
 */
function tast(n) {
    //alert("tasto:" + SES.x[n]);
    var k, cifra;
    if (n == "*") {
        while (SES.pin.length < 6)
            SES.pin.push("*");
    }
    else {
        cifra = SES.x[n];
        for (k = 0; k < 6; k++) {
            if ($("#p" + k).text() == "") {
                // metto un trattino nella posizione vuota del pin per segnalare che l'ho riempito
                $("#p" + k).text("-");
                // registro la cifra del pin
                SES.pin.push(cifra);
                break;
            }
            else if (SES.pin.length == k)
                // riempie il pin con l'*
                SES.pin.push("*");
        }
    }
}
