﻿# AUTENTICAZIONE TIPO ING-DIRECT

## TESTO

Riprodurre il metodo di autenticazione di ING-DIRECT

## DESCRIZIONE

Il metodo di autenticazione di **ING-DIRECT** funziona in 2 fasi:
1) la prima fase consiste nella richiesta di un codice cliente, e data di nascita; ![input codice cliente](img/help1.jpg)
2) la seconda fase consiste nella richiesta di un PIN numerico di 6 cifre; ![tastierino ING](img/help2.jpg)

La seconda fase è interessante perchè il **PIN** viene richiesto in modo incompleto. Vengono richieste solo 3 cifre su 6 in posizioni random (decise dal server). Quindi la fase 2 può essere scomposta nelle seguenti sottofasi:
- il server riceve il codice cliente;
- il server calcola random una configurazione di 3 campi di input su 6 e la invia al client.
- il client visualizza il tastierino numerico e 6 campi per il PIN, ma chiede solo 3 delle 6 cifre nelle posizioni vuote. Nelle altre 3 posizioni visualizza un asterisco. Nel tastierino appaiono le cifre da 0 a 9 in posizione casuale. Per digitare il pin si può solo usare il tastierino. Non si può usare la tastiera.
- il client restituisce 3 cifre del PIN rispetto alle 6 cifre totali alla pressione del tasto *Avanti*. Così il pin completo non transita mai nel web.

## ANALISI

### OVERVIEW

E' una applicazione web client-server. Il client è una pagina HTML resa dinamica con del codice javascript associato.
La parte grafica viene realizzata con un foglio di stile per migliorare la leggibilità e la manutenzione della pagina.
Come server si usa un programma c# racchiuso in una web-form .NET 2.0 di cui si usa solo la capacità di rispondere alle richieste HTTP
instradate da un web server IIS.

### COMPONENTI

L'applicazione risiede in una virtual directory del server web ed è composta dai seguenti file sorgente:

1) index.html (viene scaricata sul client assieme a mystyle.css, myscript.js e la libreria jquery.js)
2) mystyle.css
3) myscript.js
4) web.config (file di configurazione del server della applicazione)
5) ingserver.aspx (web form che fornisce i servizi. è il server della applicazione)
6) ingserver.aspx.cs (codice c# associato alla web form)
7) documentazione.md (questo file)

### LIBRERIE

Alla base di tutte le operazioni sul client viene usata la libreria [JQuery](http://www.jquery.com) 
che offre una serie di metodi semplici e potenti per manipolare il DOM (Document Object Model).
Per la parte grafica, oltre al foglio di stile, si può usare la libreria [w3.css](https://www.w3schools.com/w3css/), una libreria di stili pronti all'uso.
>In particolare, per richiedere al server i dati e la validazione delle credenziali, si usa la funzione **ajax** che fa parte di
jquery. Questo è uno dei metodi possibili. Ce ne sono altri.

### FUNZIONAMENTO (SPECIFICHE DI PROGETTO / USE CASES)

Per spiegare in modo sintetico le funzionalità della applicazione ci sono vari metodi. Si sceglie di usare una: 
[*TABELLA DEI CONTROLLER*](documentazione_controller_list.ods).

Il server della applicazione è nel codice c# della web form. La funzione Page_load() interpreta i comandi HTTP e richiama gli handler_

    protected void Page_Load(object sender, EventArgs e)
    {
        op = Request["op"];
        ccli = Request["ccli"];
        pin = Request["pin"];
        ctenta = Request["ctenta"];

        if (op != null)
        {
            if (op == "2")
                serverFase1(ccli);
            else if (op == "3")
                serverValidaPIN(pin, ccli, ctenta);
        }
    }

